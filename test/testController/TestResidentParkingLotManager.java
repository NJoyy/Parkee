package testController;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import controller.ResidentParkingLotManager;
import model.Owner;
import model.ResidentParkingLot;

public class TestResidentParkingLotManager {
	
	ResidentParkingLotManager rplm = new ResidentParkingLotManager();

	@Test
	public void testRegisterResidentParkingLot() {
		Owner owner = new Owner();
		owner.setId(2);
		String postCode = "01234";
		String postCodeDistrict = "Test";
		
		rplm.registerResidentParkingLot(owner, postCode, postCodeDistrict);
		
		ResidentParkingLot residentParkingLot = rplm.readResidentParkingLotByID(100001);
		assertEquals(residentParkingLot.getPostCode(), postCode);
		assertEquals(residentParkingLot.getPostCodeDistrict(), postCodeDistrict);
		
		rplm.deleteResidentParkingLotByID(100001);
	}
	
	@Test
	public void testReadResidentParkingLotByID() {
		ResidentParkingLot residentParkingLot = rplm.readResidentParkingLotByID(100000);
		
		assertEquals(residentParkingLot.getPostCode(), "02625");
		assertEquals(residentParkingLot.getPostCodeDistrict(), "Bautzen");
	}
	
	@Test
	public void testReadResidentParkingLots() {
		Owner owner = new Owner();
		owner.setId(2);
		
		ArrayList<ResidentParkingLot> residentParkingLots = rplm.readResidentParkingLots(owner);
		assertEquals(residentParkingLots.size(), 4);
	}
	
	@Test
	public void testDeleteResidentParkingLotByID() {
		Owner owner = new Owner();
		owner.setId(2);
		String postCode = "01234";
		String postCodeDistrict = "Test";
		
		rplm.registerResidentParkingLot(owner, postCode, postCodeDistrict);
		
		rplm.deleteResidentParkingLotByID(100001);
		
		ResidentParkingLot residentParkingLot = rplm.readResidentParkingLotByID(100001);
		assertEquals(residentParkingLot, null);
	}
	
	@Test
	public void testReadResidentParkingLotPostCodes() {
		String postCode = "02625";
		
		ArrayList<ResidentParkingLot> residentParkingLots = rplm.readResidentParkingLotPostCodes(postCode);
		assertEquals(residentParkingLots.size(), 1);
	}

}
