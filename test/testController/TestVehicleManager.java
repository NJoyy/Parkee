package testController;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import controller.VehicleManager;
import model.Customer;
import model.Vehicle;

public class TestVehicleManager {
	
	VehicleManager vm = new VehicleManager();

	@Test
	public void testRegisterVehicle() {
		Customer customer = new Customer();
		customer.setId(1000000);
		String licensePlate = "TestPlate";
		String vehicleType = "TestType";
		vm.registerVehicle(customer, licensePlate, vehicleType);
		
		Vehicle vehicle = vm.readVehicleByID(10000006);
		assertEquals(vehicle.getLicensePlate(), licensePlate);
		assertEquals(vehicle.getVehicleType(), vehicleType);
		
		vm.deleteVehicleByID(10000006);
	}
	
	@Test
	public void testIsLicensePlateUsed() {
		String newLicensePlate = "BZ XX XX 1234";
		String usedLicensePlate = "L 12 XY";
		
		assertFalse(vm.isLicensePlateUsed(newLicensePlate));
		assertTrue(vm.isLicensePlateUsed(usedLicensePlate));
	}
	
	@Test
	public void testReadVehicleByID() {
		Vehicle vehicle = vm.readVehicleByID(10000002);
		
		assertEquals(vehicle.getLicensePlate(), "L 12 XY");
		assertEquals(vehicle.getVehicleType(), "Audi A5");
	}
	
	@Test
	public void testReadVehicles() {
		Customer customer = new Customer();
		customer.setId(1000000);
		ArrayList<Vehicle> vehicles = vm.readVehicles(customer);
		
		assertEquals(vehicles.size(), 3);
	}
	
	@Test
	public void testDeleteVehicleByID() {
		Customer customer = new Customer();
		customer.setId(1);
		vm.registerVehicle(customer, "testplate", "testtype");
		
		vm.deleteVehicleByID(10000006);
		
		Vehicle vehicle = vm.readVehicleByID(10000006);
		assertEquals(vehicle, null);
	}

}
