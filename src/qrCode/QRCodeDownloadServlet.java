package qrCode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Customer;

/**
 * 
 * @author fabianM
 *
 */
@WebServlet("/QRCodeDownload")
public class QRCodeDownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Customer customer = (Customer) request.getSession().getAttribute("customer");
		int customerID = customer.getId();

		File qrCodeFile = new File("D:\\home\\site\\wwwroot\\qr-code\\QRcode-" + customerID + ".png");
		
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", qrCodeFile.getName()));
		
		OutputStream out = response.getOutputStream();
		try (FileInputStream in = new FileInputStream(qrCodeFile)){
			byte[] buffer = new byte[4096];
			int length = -1;
			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}
		}
		out.flush();	
	}

}
