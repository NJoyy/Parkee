package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import data.Database;
import model.ResidentArea;

/**
 * 
 * @author fabianM
 *
 */
public class ResidentAreaManager {
	
	/**
	 * registers parking area to database
	 * @param residentParkingLot
	 * @param areaNumber
	 * @param areaName
	 * @param parkingSpaces
	 */
	public void registerResidentArea(int residentParkingLotID, int areaNumber, 
										String areaName, int parkingSpaces) {
		
		if (residentParkingLotID != 0 && areaNumber != 0 && areaName != null && parkingSpaces != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					//creates new resident area id
					int id = 200000;
					int maxID = 0;
					String readMaxIdSQL = "SELECT max(ResidentParkingLotAreaID) "
										+ "FROM ResidentParkingLotArea";
					rs = stm.executeQuery(readMaxIdSQL);
					while (rs.next()) {
						maxID = rs.getInt("max(ResidentParkingLotAreaID)");
					}
					if (maxID >= id) {
						id = maxID + 1;
					}
					
					//inserts resident area into database
					String insertResidentAreaSQL = "INSERT INTO ResidentParkingLotArea "
												+ "(ResidentParkingLotAreaID, ResidentParkingLotID, "
												+ "AreaNumber, AreaName, ParkingSpaces) "
												+ "VALUES (" + id + ", "
												+ residentParkingLotID + ", "
												+ areaNumber + ", "
												+ "'" + areaName + "', "
												+ parkingSpaces + ")";
					stm.executeUpdate(insertResidentAreaSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * reads out resident area from database and returns its object
	 * @param residentAreaID
	 * @return
	 */
	public ResidentArea readResidentAreaByID(int residentAreaID) {
		
		if (residentAreaID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readResidentAreaSQL = "SELECT ResidentParkingLotID, AreaNumber, "
											   + "AreaName, ParkingSpaces "
											   + "FROM ResidentParkingLotArea "
											   + "WHERE ResidentParkingLotAreaID "
											   + "= " + residentAreaID;
					rs = stm.executeQuery(readResidentAreaSQL);
					while (rs.next()) {
						ResidentArea residentArea = new ResidentArea();
						
						//sets main data
						residentArea.setId(residentAreaID);
						residentArea.setAreaNumber(rs.getInt("AreaNumber"));
						residentArea.setAreaName(rs.getString("AreaName"));
						residentArea.setParkingSpaces(rs.getInt("ParkingSpaces"));
						
						//sets resident parking lot data
						ResidentParkingLotManager rplm = new ResidentParkingLotManager();
						residentArea.setResidentParkingLot(rplm.readResidentParkingLotByID(rs.getInt("ResidentParkingLotID")));
						
						return residentArea;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * reads out resident parking lot�s registrated resident areas from database
	 * returns an array with them
	 * @param residentParkingLot
	 * @return
	 */
	public ArrayList<ResidentArea> readResidentAreas(int residentParkingLotID) {
		
		if (residentParkingLotID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					ArrayList<ResidentArea> residentAreas = new ArrayList<>();
					
					String readParkingAreaSQL = "SELECT ResidentParkingLotAreaID, "
											  + "AreaNumber, AreaName, ParkingSpaces "
											  + "FROM ResidentParkingLotArea "
											  + "WHERE ResidentParkingLotID "
											  + "= " + residentParkingLotID;
					rs = stm.executeQuery(readParkingAreaSQL);
					while (rs.next()) {
						ResidentArea residentArea = new ResidentArea();
						
						//sets main data
						residentArea.setId(rs.getInt("ResidentParkingLotAreaID"));
						residentArea.setAreaNumber(rs.getInt("AreaNumber"));
						residentArea.setAreaName(rs.getString("AreaName"));
						residentArea.setParkingSpaces(rs.getInt("ParkingSpaces"));
						
						residentAreas.add(residentArea);
					}
					return residentAreas;
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
		}
		return null;
	}

	/**
	 * deletes resident area and it�s data from database
	 * @param residentArea
	 */
	public void deleteResidentAreaByID(int residentAreaID) {
		
		if (residentAreaID != 0) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteResidentAreaSQL = "DELETE FROM ResidentParkingLotArea "
												 + "WHERE ResidentParkingLotAreaID "
												 + "= " + residentAreaID;
					stm.executeUpdate(deleteResidentAreaSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * deletes all of resident parking lot�s resident areas and their data from database
	 * @param residentParkingLot
	 */
	public void deleteAllResidentAreasByID(int residentParkingLotID) {
		
		if (residentParkingLotID != 0) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteResidentAreaSQL = "DELETE FROM ResidentParkingLotArea "
											     + "WHERE ResidentParkingLotAreaID "
											     + "= " + residentParkingLotID;
					stm.executeUpdate(deleteResidentAreaSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}

}
