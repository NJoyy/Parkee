<%@page import="controller.ResidentParkingLotManager"%>

<!DOCTYPE HTML>

<!-- checks if the user is logged in as owner -->
<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	String parkingLotIDinput = request.getParameter("parkingLotID");
	int parkingLotID = Integer.parseInt(parkingLotIDinput);
	ResidentParkingLotManager rplm = new ResidentParkingLotManager();
	rplm.deleteResidentParkingLotByID(parkingLotID);
%>

<jsp:forward page="owner_ParkingLot.jsp" />