package model;

import java.util.ArrayList;

/**
 * 
 * @author fabianM
 *
 */
public class ResidentArea {
	
	//attributes
	private int id;
	private ResidentParkingLot residentParkingLot;
	private int areaNumber;
	private String areaName;
	private int parkingSpaces;
	private ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
	
	//getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public ResidentParkingLot getResidentParkingLot() {
		return residentParkingLot;
	}
	public void setResidentParkingLot(ResidentParkingLot residentParkingLot) {
		this.residentParkingLot = residentParkingLot;
	}
	
	public int getAreaNumber() {
		return areaNumber;
	}
	public void setAreaNumber(int areaNumber) {
		this.areaNumber = areaNumber;
	}
	
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	public int getParkingSpaces() {
		return parkingSpaces;
	}
	public void setParkingSpaces(int parkingSpaces) {
		this.parkingSpaces = parkingSpaces;
	}

	public ArrayList<ResidentBooking> getResidentBookings() {
		return residentBookings;
	}
	public void setResidentBookings(ArrayList<ResidentBooking> residentBookings) {
		this.residentBookings = residentBookings;
	}
	
}
