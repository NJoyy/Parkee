package testController;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import controller.ResidentAreaManager;
import model.ResidentArea;

public class TestResidentAreaManager {
	
	ResidentAreaManager ram = new ResidentAreaManager();

	@Test
	public void testRegisterResidentArea() {
		int parkingLotID = 100000;
		int areaNumber = 999;
		String areaName = "test";
		int parkingSpaces = 123;
		
		ram.registerResidentArea(parkingLotID, areaNumber, areaName, parkingSpaces);
		
		ResidentArea residentArea = ram.readResidentAreaByID(200002);
		assertEquals(residentArea.getResidentParkingLot().getId(), parkingLotID);
		assertEquals(residentArea.getAreaNumber(), areaNumber);
		assertEquals(residentArea.getAreaName(), areaName);
		assertEquals(residentArea.getParkingSpaces(), parkingSpaces);
		
		ram.deleteResidentAreaByID(200002);
	}
	
	@Test
	public void testReadResidentAreaByID() {
		ResidentArea residentArea = ram.readResidentAreaByID(200000);
		
		assertEquals(residentArea.getResidentParkingLot().getId(), 100000);
		assertEquals(residentArea.getAreaNumber(), 1);
		assertEquals(residentArea.getAreaName(), "Bautzen-Ost");
		assertEquals(residentArea.getParkingSpaces(), 200);
	}
	
	@Test
	public void testReadResidentAreas() {
		ArrayList<ResidentArea> residentAreas = ram.readResidentAreas(100000);
		assertEquals(residentAreas.size(), 2);
	}
	
	@Test
	public void testDeleteResidentAreaByID() {
		ram.registerResidentArea(100000, 123, "test", 100);
		
		ram.deleteResidentAreaByID(200002);
		ResidentArea residentArea = ram.readResidentAreaByID(200002);
		assertEquals(residentArea, null);
	}
	
}
