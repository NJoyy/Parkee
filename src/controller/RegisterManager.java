package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import data.Database;
import qrCode.QRCodeGenerator;

/**
 * includes methods to registrate user
 * @author fabianM
 *
 */
public class RegisterManager {
	
	/**
	 * registers customer to the database as a parkee user
	 * creates a personalized QR code file and puts it into directory "QR-Codes"
	 * @param name
	 * @param email
	 * @param passwd
	 */
	public void registerCustomer(String name, String email, String passwd) {
		
		if (name != null && email != null && passwd != null) {
			Statement stm = Database.getStatement();;
			ResultSet rs = null;
			try {
				//creates new customer ID
				int id = 1000000;
				int maxID = 0;
				String readMaxIdSQL = "SELECT max(CustomerID) "
									+ "FROM Customer";
				rs = stm.executeQuery(readMaxIdSQL);
				while (rs.next()) {
					maxID = rs.getInt("max(CustomerID)");
				}
				if (maxID >= id) {
					id = maxID + 1;
				}
				
				//inserts customer into database
				String insertCustomerSQL = "INSERT INTO Customer "
										 + "(CustomerID, CustomerName, Email, Password) "
										 + "VALUES (" + id + ", '" + name + "', "
										 + "'" + email + "', '" + passwd + "')";
				stm.executeUpdate(insertCustomerSQL);
				
				//creates customers personalized QR-code
				QRCodeGenerator qrCodeGenerator = new QRCodeGenerator();
				qrCodeGenerator.createQRCodeFile(id);
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {Database.getConnection().close();} catch (SQLException e) {}
				try {stm.close();} catch (SQLException e) {}
				try {rs.close();} catch (SQLException e) {}
			}
		}
	}
	
	/**
	 * registers owner to the database as a parkee user
	 * @param name
	 * @param email
	 * @param passwd
	 */
	public void registerOwner(String name, String email, String passwd) {
		
		if (name != null && email != null && passwd != null) {
			Statement stm = Database.getStatement();;
			ResultSet rs = null;
			try {
				//creates new owner ID
				int id = 8000000;
				int maxID = 0;
				String readMaxIdSQL = "SELECT max(OwnerID) "
									+ "FROM Owner";
				rs = stm.executeQuery(readMaxIdSQL);
				while (rs.next()) {
					maxID = rs.getInt("max(OwnerID)");
				}
				if (maxID >= id) {
					id = maxID + 1;
				}
				
				//inserts owner into database
				String insertOwnerSQL = "INSERT INTO Owner "
									  + "(OwnerID, OwnerName, Email, Password) "
									  + "VALUES (" + id + ", '" + name + "', "
									  + "'" + email + "', '" + passwd + "')";
				stm.executeUpdate(insertOwnerSQL);
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {Database.getConnection().close();} catch (SQLException e) {}
				try {stm.close();} catch (SQLException e) {}
				try {rs.close();} catch (SQLException e) {}
			}
		}
	}
	
	/**
	 * checks if email is still used in database
	 * @param email
	 * @return
	 */
	public boolean isEmailUsed(String email) {
		if (email != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readCustomerSQL = "SELECT Email "
										   + "FROM Customer "
										   + "WHERE Email = '" + email + "'";
					ResultSet rs1 = stm.executeQuery(readCustomerSQL);
					if (rs1.next() == true) {
						return true;
					} else {
						String readOwnerSQL = "SELECT Email "
										    + "FROM Owner "
										    + "WHERE Email = '" + email + "'";
						ResultSet rs2 = stm.executeQuery(readOwnerSQL);
						if (rs2.next() == true) {
							return true;
						}
						else {
							return false;
						}
					} 
				} catch(SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
		return false;
	}
}
