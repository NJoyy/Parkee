<!DOCTYPE HTML>

<html>
<head>
	<meta charset="UTF-8" />
	
	<title>Parkee - Registrierung Parkbereich</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/login.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,
	400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>

<body>
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

	<div class="container">
		<section>
			<div id="container_demo">
				<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
				<a class="hiddenanchor"></a>
				<div id="wrapper">
					<div id="signupParkingArea" class="animate form">
						<form action="owner_SignUpParkingArea.jsp" autocomplete="on"
							method="post">
							<h1>Parkbereich registrieren</h1>
							<p>Geben Sie bitte hier die Informationen zum Parkbereich
								ein.</p>
							<p>
								<label for="areaNumber" class="areaNumber">Parkbereich-Nummer</label>
								<input id="areaNumber" name="areaNumber" required="required"
									type="text" pattern="[0-9.]+"
									placeholder="Bitte Nummer des Parkbereiches angeben .." />
							</p>
							<p>
								<label for="areaName" class="areaName">Name des
									Parkbereiches</label> <input id="areaName" name="areaName"
									required="required" type="text"
									placeholder="Bitte Name des Parkbereiches angeben .." />
							</p>
							<p>
								<label for="parkingSpaces" class="parkingSpaces">Anzahl
									Parkpl�tze</label> <input id="parkingSpaces" name="parkingSpaces"
									required="required" type="text" pattern="[0-9.]+"
									placeholder="Bitte Anzahl der verf�gbaren Parkpl�tze angeben .." />
							</p>
							<input type="hidden" name="parkingLotID"
								value='<%=request.getParameter("parkingLotID")%>'> <input
								type="hidden" name="parkingLotPostCode"
								value='<%=request.getParameter("parkingLotPostCode")%>'>
							<input type="hidden" name="parkingLotPostCodeDistrict"
								value='<%=request.getParameter("parkingLotPostCodeDistrict")%>'>
							<br>
							<p class="ParkingAreaSignUpButton">
								<input type="submit" value="Parkbereich registrieren"
									target="_blank">
							</p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>

</html>