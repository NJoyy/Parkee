<%@page import="model.Owner"%>

<!Doctype HTML>

<%-- checks if the user is logged in as owner --%>
<%
	Owner owner = (Owner) request.getSession().getAttribute("owner");
	if (owner == null) {
	%>
		<jsp:forward page="frontpage.html"></jsp:forward>
	<%
	}
%>