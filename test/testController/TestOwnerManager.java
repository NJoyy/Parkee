package testController;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.OwnerManager;
import model.Owner;

public class TestOwnerManager {

	OwnerManager om = new OwnerManager();

	@Test
	public void testReadOwnerID() {
		Owner owner = om.readOwnerByID(2);
		
		assertEquals(owner.getName(), "testowner");
		assertEquals(owner.getEmail(), "testowner@test.com");
		assertEquals(owner.getPasswd(), "lala12");
	}
	
	@Test
	public void testUpdateOwner() {
		Owner owner = om.readOwnerByID(8);
		
		String newName = "TestName";
		String newEmail = "testedowner@test.com";
		Owner newOwner = om.updateOwner(owner, newName, newEmail);
		
		Owner updatedOwner = om.readOwnerByID(newOwner.getId());
		
		assertNotEquals(updatedOwner.getName(), "owner");
		assertNotEquals(updatedOwner.getEmail(), "testowner@test.com");
	}
	
	@Test
	public void testUpdateOwnerPassword() {
		Owner owner = om.readOwnerByID(8);
		
		String newPassword = "secret";
		Owner newCustomer = om.updateOwnerPassword(owner, newPassword);
		
		Owner updatedOwner = om.readOwnerByID(newCustomer.getId());
		
		assertEquals(updatedOwner.getPasswd(), "secret");
		assertNotEquals(updatedOwner.getPasswd(), "lala12");
	}
	
	@Test
	public void testDeleteOwner() {
		Owner owner = om.readOwnerByID(11);
		om.deleteOwner(owner);
		
		Owner deletedOwner = om.readOwnerByID(1);
		
		assertEquals(deletedOwner, null);
	}

}
