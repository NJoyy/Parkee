<%@page import="controller.ResidentBookingManager"%>
<%@page import="model.ResidentBooking"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.CustomerManager"%>
<%@page import="model.Customer"%>
<!DOCTYPE HTML>

<html>
<head>
	<title>Parkee - Parkscheinkontrolle</title>
	
	<!-- CSS-Files -->
	<link href="css/stylish-portfolio.css" rel="stylesheet" type="text/css">
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet"
		type="text/css">
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css"
		rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="vendor/datatables/dataTables.bootstrap4.css"
		rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
</head>
<body>
	<div style="color: white; background-color: black; padding-left: 20px">
		<h1>Parkee - Parkscheinkontrolle</h1>
		<h2 style="padding-bottom: 15px">Bewohnerparkplatzbuchungen</h2>
	</div>
	<br>
	<%
		String customerIDparameter = request.getParameter("id");
		if (customerIDparameter == null) {
		%>
			<div class="alert alert-danger">
				<h3 style="color: red">
					<strong>Achtung!</strong> Es wurde keine ID gefunden.
				</h3>
			</div>
		<%
		} else {
			int customerID = Integer.parseInt(customerIDparameter);

			CustomerManager cm = new CustomerManager();
			Customer customer = cm.readCustomerByID(customerID);

			if (customer != null) {
			%>
				<p style="margin-left: 30px"><b>Kundenname:</b><%=customer.getName()%></p>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="checkQR">
							<thead>
								<tr>
									<th>Kennzeichen</th>
									<th>Fahrzeugtyp</th>
									<th>Parkplatz</th>
									<th>G�ltigkeit von</th>
									<th>G�ltigkeit bis</th>
								</tr>
							</thead>
							<tbody>
							<%
								ResidentBookingManager rbm = new ResidentBookingManager();
								ArrayList<ResidentBooking> residentBookings = rbm.readResidentBookings(customer);
								if (residentBookings.isEmpty()) {
								%>
									<div class="alert alert-danger">
										<h3 style="color: red">Der Kunde hat bisher keine Buchungen get�tigt.</h3>
									</div>
								<%
								} else {
									for (ResidentBooking residentBooking : residentBookings) {
									%>
										<tr>
											<td><%=residentBooking.getVehicle().getLicensePlate()%></td>
											<td><%=residentBooking.getVehicle().getVehicleType()%></td>
											<td><%=residentBooking.getResidentArea().getAreaName()%></td>
											<td><%=residentBooking.getDate()%></td>
											<td><%=residentBooking.getEndDate()%></td>
										</tr>
									<%
									}
								}
							%>
							</tbody>
						</table>
					</div>
				</div>
			<%
			} else {
			%>
				<h3 style="color: red">
					<strong>Achtung!</strong> Die angegebene ID ist ung�ltig.
				</h3>
			<%
			}
		}
	%>
	
	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
	
	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js" type="text/javascript"></script>
	
	<!-- Page level plugin JavaScript-->
	<script src="vendor/chart.js/Chart.min.js" type="text/javascript"></script>
	<script src="vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
	
	<!-- Custom scripts for all pages-->
	<script src="vendor/js/sb-admin.min.js" type="text/javascript"></script>
	
	<!-- Custom scripts for this page-->
	<script src="vendor/js/sb-admin-datatables.min.js" type="text/javascript"></script>
	<script src="vendor/js/sb-admin-charts.min.js" type="text/javascript"></script>
	
	<!-- Table in German language -->
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#checkQR').DataTable({
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
				}
			});
		});
	</script>
</body>

</html>