<%@page import="controller.CustomerManager"%>
<%@page import="model.Customer"%>

<!DOCTYPE HTML>
<head>
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">
</head>
	 
<%	
	Customer customer = (Customer) request.getSession().getAttribute("customer");
	String name = request.getParameter("name");
	String email = request.getParameter("email");
		
	CustomerManager cm = new CustomerManager();
	
	Customer newCustomer = cm.updateCustomer(customer, name, email);
	request.getSession().setAttribute("customer", newCustomer);
%>

<jsp:forward page="customer_Profile.jsp"></jsp:forward>