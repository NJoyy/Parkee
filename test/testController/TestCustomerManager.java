package testController;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.CustomerManager;
import model.Customer;

public class TestCustomerManager {
	
	CustomerManager cm = new CustomerManager();

	@Test
	public void testReadCustomerByID() {
		int customerID = 1000000;
		Customer customer = cm.readCustomerByID(customerID);
		
		assertEquals(customer.getName(), "TestCustomer");
		assertEquals(customer.getEmail(), "testcustomer@test.com");
		assertEquals(customer.getPasswd(), "lala12");
	}
	
	@Test
	public void testReadCustomerByEmail() {
		String email = "testcustomer@test.com";
		Customer customer = cm.readCustomerByEmail(email);
		
		assertEquals(customer.getId(), 1000000);
		assertEquals(customer.getName(), "TestCustomer");
		assertEquals(customer.getPasswd(), "lala12");
	}
	
	@Test
	public void testUpdateCustomer() {
		Customer customer = cm.readCustomerByID(1000001);
		
		String newName = "TestName";
		String newEmail = "testedcustomer@test.com";
		Customer newCustomer = cm.updateCustomer(customer, newName, newEmail);
		
		Customer updatedCustomer = cm.readCustomerByID(newCustomer.getId());
		
		assertNotEquals(updatedCustomer.getName(), "TestCustomer");
		assertNotEquals(updatedCustomer.getEmail(), "testcustomer@test.com");
	}
	
	@Test
	public void testUpdateCustomerPassword() {
		Customer customer = cm.readCustomerByID(1000001);
		
		String newPassword = "secret";
		Customer newCustomer = cm.updateCustomerPassword(customer, newPassword);
		
		Customer updatedCustomer = cm.readCustomerByID(newCustomer.getId());
		
		assertEquals(customer.getPasswd(), "secret");
		assertNotEquals(updatedCustomer.getPasswd(), "lala12");
	}
	
	@Test
	public void testDeleteCustomer() {
		Customer customer = cm.readCustomerByID(1);
		cm.deleteCustomer(customer);
		
		Customer deletedCustomer = cm.readCustomerByID(1);
		
		assertEquals(deletedCustomer, null);
	}

}
