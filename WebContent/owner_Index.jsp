<!DOCTYPE html>
<%@page import="model.Owner"%>

<html lang="de">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>Parkee - Dashboard</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<a class="navbar-brand" href="owner_Index.jsp">Parkee</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Dashboard"><a class="nav-link" href="owner_Index.jsp">
						<i class="fa fa-fw fa-dashboard"></i> <span class="nav-link-text">Dashboard</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Parkplätze"><a class="nav-link"
					href="owner_ParkingLot.jsp"> <i class="fa fa-fw fa-table"></i>
						<span class="nav-link-text">Meine Parkpl�tze</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Parkplätze"><a class="nav-link"
					href="owner_Earnings.jsp"> <i class="fa fa-fw fa-table"></i> <span
						class="nav-link-text">Meine Eink�nfte</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Profildaten"><a class="nav-link"
					href="owner_Profile.jsp"> <i class="fa fa-fw fa-wrench"></i> <span
						class="nav-link-text">Mein Profil</span>
				</a></li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item"><a class="nav-link text-center"
					id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
				</a></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" data-toggle="modal"
					data-target="#exampleModal"> <i class="fa fa-fw fa-sign-out"></i>Logout
				</a></li>
			</ul>
		</div>
	</nav>

	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item" style="font-size: 18px">
					<%
						Owner owner = (Owner) request.getSession().getAttribute("owner");
						out.write("Willkommen " + owner.getName() + "!");
					%>
				</li>
			</ol>

			<!-- Icon Cards-->
			<div class="row">
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-success o-hidden h-100">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-list"></i>
							</div>
							<div class="mr-5">Ihr Profil</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#"
							onclick="self.location.href='owner_Profile.jsp'"> <span
							class="float-left">Details</span> <span class="float-right">
								<i class="fa fa-angle-right"></i>
						</span>
						</a>
					</div>
				</div>
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-warning o-hidden h-100">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-list"></i>
							</div>
							<div class="mr-5">Ihre Parkpl�tze</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#"
							onclick="self.location.href='owner_ParkingLot.jsp'"> <span
							class="float-left">Details</span> <span class="float-right">
								<i class="fa fa-angle-right"></i>
						</span>
						</a>
					</div>
				</div>
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="card text-white bg-info o-hidden h-100">
						<div class="card-body">
							<div class="card-body-icon">
								<i class="fa fa-fw fa-list"></i>
							</div>
							<div class="mr-5">Ihre Eink�nfte</div>
						</div>
						<a class="card-footer text-white clearfix small z-1" href="#"
							onclick="self.location.href='owner_Earnings.jsp'"> <span
							class="float-left">Details</span> <span class="float-right">
								<i class="fa fa-angle-right"></i>
						</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div style="margin-left: 25px; margin-top: 50px; margin-right: 25px; font-size: 18px; background-color: #f5f5f5; 
		padding: 15px; border-radius: 20px">
			<p>Sch�n, dass Sie sich als Parkplatzbetreiber f�r Parkee angemeldet haben!</p>
			<p>Parkee ist ein dynamisches Parkplatzverwaltungs-Tool, mit dessen Funktionalit�ten Parkpl�tze 
			�ber das Internet gemietet und vermietet werden k�nnen. Die Kontrolle erfolgt �ber den pers�nlichen QR-Code des Kunden, welcher 
			in dessen Auto angebracht wird.</p>
			<p>Wir m�chten Sie darauf aufmerksam machen, dass Parkee sich in einem Demo-Status befindet. 
			Dies bedeutet, dass diese Webanwendung unvollst�ndig in ihrer Funktionalit�t ist und sich <b>(Wichtig!)</b> 
			momentan <b>nicht</b> auf die Realit�t anwenden l�sst. Dennoch sind Sie herzlich eingeladen, 
			die einzelnen Funktionalit�ten auszuprobieren.</p>
			<p>Bei n�herem Interesse melden Sie sich doch bitte <a href="mailto:paulkonietzny@gmail.com">hier</a> oder 
			<a href="mailto:fabian.mittmann@gmail.com">hier</a>.
			<p>Wir w�nschen einen sch�nen Tag!<br>
			Die Parkee-Admins</p>
		</div><br><br>
		
		</div>

		<!-- /.container-fluid-->

		<section id="contact" class="map">
			<iframe width="100%" height="400px"
				src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2501.021156741215!2d14.433604715758069!3d51.18183297958289!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4708558838fd01d3%3A0x136e7c33a83d101f!2sStaatliche+Studienakademie+Bautzen!5e0!3m2!1sde!2sde!4v1511515614287"></iframe>
			<br /> <small> <a
				href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2501.021156741215!2d14.433604715758069!3d51.18183297958289!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4708558838fd01d3%3A0x136e7c33a83d101f!2sStaatliche+Studienakademie+Bautzen!5e0!3m2!1sde!2sde!4v1511515614287"></a>
			</small>
		</section>
		<!-- /.content-wrapper-->
		<footer class="sticky-footer">
			<div class="container">
				<div class="text-center">
					<small>Parkee</small>
				</div>
			</div>
		</footer>
		
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top"> <i
			class="fa fa-angle-up"></i>
		</a>
		
		<!-- Logout Modal-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ausloggen?</h5>
						<button class="close" type="button" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">�</span>
						</button>
					</div>
					<div class="modal-body">Wollen Sie sich ausloggen?</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button"
							data-dismiss="modal">Abbrechen</button>
						<a class="btn btn-primary" href="logout.jsp">Ausloggen</a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Bootstrap core JavaScript-->
		<script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
		
		<!-- Core plugin JavaScript-->
		<script src="vendor/jquery-easing/jquery.easing.min.js" type="text/javascript"></script>
		
		<!-- Page level plugin JavaScript-->
		<script src="vendor/chart.js/Chart.min.js" type="text/javascript"></script>
		<script src="vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
		<script src="vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
		
		<!-- Custom scripts for all pages-->
		<script src="vendor/js/sb-admin.min.js" type="text/javascript"></script>
		
		<!-- Custom scripts for this page-->
		<script src="vendor/js/sb-admin-datatables.min.js" type="text/javascript"></script>
		<script src="vendor/js/sb-admin-charts.min.js" type="text/javascript"></script>
</body>

</html>
