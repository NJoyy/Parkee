package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import data.Database;
import model.Customer;

/**
 * 
 * @author fabianM
 *
 */
public class CustomerManager {
	
	/**
	 * reads out customer from database and returns its object by searching for its id
	 * @param vehicleID
	 * @return
	 */
	public Customer readCustomerByID(int customerID) {
		if (customerID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readCustomerSQL = "SELECT CustomerName, Email, Password "
										   + "FROM Customer "
										   + "WHERE CustomerID = " + customerID;
					rs = stm.executeQuery(readCustomerSQL);
					while (rs.next()) {
						//sets main data
						Customer customer = new Customer();
						customer.setId(customerID);
						customer.setName(rs.getString("CustomerName"));
						customer.setEmail(rs.getString("Email"));
						customer.setPasswd(rs.getString("Password"));
						
						return customer;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
		}
		return null;
	}
	
	/**
	 * reads out customer from database and returns its object by searching for his email
	 * @param vehicleID
	 * @return
	 */
	public Customer readCustomerByEmail(String email) {
		if (email != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readCustomerSQL = "SELECT CustomerID, CustomerName, Email, Password "
										   + "FROM Customer "
										   + "WHERE Email = '" + email + "'";
					rs = stm.executeQuery(readCustomerSQL);
					while (rs.next()) {
						//sets main data
						Customer customer = new Customer();
						customer.setId(rs.getInt("CustomerID"));
						customer.setName(rs.getString("CustomerName"));
						customer.setEmail(rs.getString("Email"));
						customer.setPasswd(rs.getString("Password"));
						
						//sets data of the vehicles of the customer
						VehicleManager vm = new VehicleManager();
						customer.setVehicles(vm.readVehicles(customer));
						
						return customer;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * updates customer's data in database
	 * returns user so his new data could set as session attribute 
	 * @param customer
	 * @param name
	 * @param email
	 * @param passwd
	 * @return
	 */
	public Customer updateCustomer(Customer customer, String name, String email) {
		
		if (customer != null && name != null && email != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//updates customer�s data in database
					String updateCustomerSQL = "UPDATE Customer "
											 + "SET CustomerName = '" + name + "', "
											 + "Email = '" + email + "' "
											 + "WHERE CustomerID = " + customer.getId();
					stm.executeUpdate(updateCustomerSQL);
					
					//updates customer object�s data
					customer = readCustomerByEmail(email);

					return customer;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * updates customer's password in database
	 * returns user so his new data could set as session attribute  
	 * @param customer
	 * @param newPassword
	 * @return
	 */
	public Customer updateCustomerPassword(Customer customer, String newPassword) {
		
		if (newPassword != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//updates customer�s data in database
					String updateCustomerSQL = "UPDATE Customer "
											 + "SET Password = '" + newPassword + "' "
											 + "WHERE CustomerID = " + customer.getId();
					stm.executeUpdate(updateCustomerSQL);
					
					//updates customer object�s data
					customer = readCustomerByEmail(customer.getEmail());

					return customer;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}

	/**
	 * deletes customer and it�s data from database
	 * @param customer
	 */
	public void deleteCustomer(Customer customer) {
		
		if (customer != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//deletes main data
					String deleteCustomerSQL = "DELETE FROM Customer "
											 + "WHERE CustomerID = " + customer.getId();
					stm.executeUpdate(deleteCustomerSQL);
					
					//deletes all vehicles from customer
					VehicleManager vm = new VehicleManager();
					vm.deleteAllVehicles(customer);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
}
