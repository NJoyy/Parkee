package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {

	//setting connection to null
	private static Connection con = null;
	private static Statement stm = null;
	
	//connect to Parkee database
	public static Connection getConnection() {
		
		String dbURL = "jdbc:mysql://dd25318.kasserver.com/d0288bf3";
		
        try {
			if (con != null && !con.isClosed()) {
			    return con;
			} else {
			    try {
			    	Class.forName("com.mysql.jdbc.Driver");
			        con = DriverManager.getConnection(dbURL,"d0288bf3","T2t877oa2s7zUgdL");
			        return con;
			    } catch(SQLException sqle) {
			    	System.out.println("An error occured while connecting to Parkee-Database.");
			        System.out.println(sqle.getStackTrace());
				} catch(ClassNotFoundException cnfe) {
			    	System.out.println("MySQl-Class could not be found.");
			        System.out.println(cnfe.getStackTrace());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
        return null;
    }
	    
	//create Statement for database actions
    public static Statement getStatement() {
        con = Database.getConnection();
        if (con != null) {
            try {
                stm = con.createStatement();
                return stm;
            } catch(SQLException sqle) {
            	System.out.println("Statement creation failed.");
                System.out.println(sqle.getMessage());
            }
        }
        return null;
    }
    
}
