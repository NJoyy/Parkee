<%@page import="controller.VehicleManager"%>
<%@page import="model.Customer"%>

<!DOCTYPE HTML>

<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">

<!-- checks if the user is logged in as customer -->
<jsp:include page="customer_CheckLogin.jsp"></jsp:include>

<%                               
	Customer customer = (Customer) request.getSession().getAttribute("customer");       
	String licensePlate = request.getParameter("licensePlate");
	String vehicleType = request.getParameter("vehicleType");
	
	VehicleManager vm = new VehicleManager();
	
	boolean licensePlateUsed = vm.isLicensePlateUsed(licensePlate);
	
	if (licensePlateUsed == true) {
		%>
		<div class="alert alert-danger">
			<h3 style="color: red"><strong>Achtung!</strong> Dieses Kennzeichen ist bereits in unserem System vergeben.</h3>
	 		<p style="font-size: 20">Bitte pr�fen Sie Ihre Eingabe.</p>
		</div>
		<%
	} else {
		vm.registerVehicle(customer, licensePlate, vehicleType);  	
		%>
		<jsp:forward page="customer_Vehicles.jsp"></jsp:forward>
		<%
	}                             
%>                     