<%@page import="model.ResidentParkingLot"%>
<%@page import="model.ResidentArea"%>
<%@page import="controller.ResidentAreaManager"%>

<!DOCTYPE HTML>

<!-- checks if the user is logged in as owner -->
<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	String parkingAreaIDinput = request.getParameter("areaID");
	int areaID = Integer.parseInt(parkingAreaIDinput);
	ResidentAreaManager ram = new ResidentAreaManager();
	ram.deleteResidentAreaByID(areaID);
%>

<jsp:forward page="owner_ParkingLotAreas.jsp" />