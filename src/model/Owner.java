package model;

import java.util.ArrayList;

/**
 * 
 * @author fabianM
 *
 */
public class Owner extends ParkeeUser {
	
	//attributes
	private ArrayList<ResidentParkingLot> residentParkingLots = new ArrayList<>();
	private ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
	
	//getters and setters
	public ArrayList<ResidentParkingLot> getResidentParkingLots() {
		return residentParkingLots;
	}
	public void setResidentParkingLots(ArrayList<ResidentParkingLot> residentParkingLots) {
		this.residentParkingLots = residentParkingLots;
	}
	
	public ArrayList<ResidentBooking> getResidentBookings() {
		return residentBookings;
	}
	public void setResidentBookings(ArrayList<ResidentBooking> residentBookings) {
		this.residentBookings = residentBookings;
	}
}
