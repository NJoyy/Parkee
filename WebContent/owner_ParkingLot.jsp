<%@page import="model.ResidentParkingLot"%>
<%@page import="controller.ResidentParkingLotManager"%>
<%@page import="model.Owner"%>
<%@page import="model.Vehicle"%>
<%@page import="model.Customer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.VehicleManager"%>

<!DOCTYPE html>

<html lang="de">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport"
		content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>Parkee - Parkplätze</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<a class="navbar-brand" href="owner_Index.jsp">Parkee</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Dashboard"><a class="nav-link" href="owner_Index.jsp">
						<i class="fa fa-fw fa-dashboard"></i> <span class="nav-link-text">Dashboard</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Fahrzeuge"><a class="nav-link"
					href="owner_ParkingLot.jsp"> <i class="fa fa-fw fa-table"></i>
						<span class="nav-link-text">Meine Parkplätze</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Fahrzeuge"><a class="nav-link"
					href="owner_Earnings.jsp"> <i class="fa fa-fw fa-table"></i> <span
						class="nav-link-text">Meine Einkünfte</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Profildaten"><a class="nav-link"
					href="owner_Profile.jsp"> <i class="fa fa-fw fa-wrench"></i> <span
						class="nav-link-text">Mein Profil</span>
				</a></li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item"><a class="nav-link text-center"
					id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
				</a></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" data-toggle="modal"
					data-target="#exampleModal"> <i class="fa fa-fw fa-sign-out"></i>Logout
				</a></li>
			</ul>
		</div>
	</nav>
	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="owner_Index.jsp">Dashboard</a>
				</li>
				<li class="breadcrumb-item active">Ihre Parkplätze</li>
			</ol>
		</div>

		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-table"></i> Parkplatzübersicht
			</div>
			<div class="card-body">
				<input type="button" class="btn btn-success"
					onClick="self.location.href='owner_SignUpParkingLotForm.jsp'"
					value="Parkplatz anmelden" style="margin-left: 15px"> <br>
				<br>
				<div class="table-responsive">
					<table class="table table-bordered" id="parkinglot">
						<thead>
							<tr>
								<th>Postleitzahl</th>
								<th>Ort</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<%
								Owner owner = (Owner) request.getSession().getAttribute("owner");
								ResidentParkingLotManager rplm = new ResidentParkingLotManager();
								ArrayList<ResidentParkingLot> parkinglots = rplm.readResidentParkingLots(owner);
								try {
									for (ResidentParkingLot parkingLot : parkinglots) {
									%>
										<tr>
											<td><%=parkingLot.getPostCode()%></td>
											<td><%=parkingLot.getPostCodeDistrict()%></td>
											<td>
												<form action="owner_ParkingLotAreas.jsp" method="post">
													<input type="hidden" name="parkingLotID"
														value="<%=parkingLot.getId()%>"> <input type="hidden"
														name="parkingLotPostCode"
														value="<%=parkingLot.getPostCode()%>"> <input
														type="hidden" name="parkingLotPostCodeDistrict"
														value="<%=parkingLot.getPostCodeDistrict()%>"> <input
														type="submit" value="Verwalten" class="btn btn-warning"
														id="administrateParkinglot">
												</form>
											</td>
											<td>
												<form action="owner_DeleteParkingLot.jsp" method="post">
													<input type="hidden" name="parkingLotID"
														value="<%=parkingLot.getId()%>"> <input type="submit"
														value="Löschen" class="btn btn-danger" id="deleteParkingLot">
												</form>
											</td>
										</tr>
									<%
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							%>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card-footer small text-muted"></div>
		</div>
	</div>

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fa fa-angle-up"></i>
	</a>
	
	<!-- Logout Modal-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ausloggen?</h5>
					<button class="close" type="button" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">—</span>
					</button>
				</div>
				<div class="modal-body">Wollen Sie sich ausloggen?</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button"
						data-dismiss="modal">Abbrechen</button>
					<a class="btn btn-primary" href="logout.jsp">Ausloggen</a>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Bootstrap core JavaScript-->
	<script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
	
	<!-- Core plugin JavaScript-->
	<script src="vendor/jquery-easing/jquery.easing.min.js" type="text/javascript"></script>
	
	<!-- Page level plugin JavaScript-->
	<script src="vendor/chart.js/Chart.min.js" type="text/javascript"></script>
	<script src="vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
	
	<!-- Custom scripts for all pages-->
	<script src="vendor/js/sb-admin.min.js" type="text/javascript"></script>
	
	<!-- Custom scripts for this page-->
	<script src="vendor/js/sb-admin-datatables.min.js" type="text/javascript"></script>
	<script src="vendor/js/sb-admin-charts.min.js" type="text/javascript"></script>

	<!-- Table in German language -->
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#checkQR').DataTable({
				"language" : {
					"url" : "//cdn.datatables.net/plug-ins/1.10.10/i18n/German.json"
				}
			});
		});
	</script>
</body>

</html>