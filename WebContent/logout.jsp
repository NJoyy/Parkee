<!DOCTYPE HTML>

<%
	session = request.getSession(false);
	if (session != null) {
		session.removeAttribute("customer");
		session.removeAttribute("owner");
	}
	if (session.getAttribute("customer") == null && session.getAttribute("owner") == null) {
		session.invalidate();
		response.sendRedirect("frontpage.html");
	}
%>