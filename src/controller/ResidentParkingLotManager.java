package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import data.Database;
import model.Owner;
import model.ResidentParkingLot;

/**
 * includes methods to sign & delete resident parking lots or to read & change its data
 * @author fabia
 *
 */
public class ResidentParkingLotManager {
	
	/**
	 * registers resident parking lot to database
	 * @param owner
	 * @param postCode
	 * @param postCodeDistrict
	 */
	public void registerResidentParkingLot(Owner owner, String postCode, String postCodeDistrict) {
		
		if (owner != null && postCode != null && postCodeDistrict != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					//creates new resident parking lot id
					int id = 100000;
					int maxID = 0;
					String readMaxIdSQL = "SELECT max(ResidentParkingLotID) "
										+ "FROM ResidentParkingLot";
					rs = stm.executeQuery(readMaxIdSQL);
					while (rs.next()) {
						maxID = rs.getInt("max(ResidentParkingLotID)");
					}
					if (maxID >= id) {
						id = maxID + 1;
					}
					
					//inserts resident parking lot into database
					String insertResidentParkingLotSQL = "INSERT INTO ResidentParkingLot "
													   + "(ResidentParkingLotID, OwnerID, "
													   + "PostCode, PostCodeDistrict) "
													   + "VALUES (" + id + ", " + owner.getId() + ", '"
													   + postCode + "', '" + postCodeDistrict + "')";
					stm.executeUpdate(insertResidentParkingLotSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * reads out resident parking lot from database and returns its object 
	 * @param residentParkingLotID
	 * @return
	 */
	public ResidentParkingLot readResidentParkingLotByID(int residentParkingLotID) {
		
		if (residentParkingLotID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readResidentParkingLotSQL = "SELECT OwnerID, PostCode, "
													 + "PostCodeDistrict "
													 + "FROM ResidentParkingLot "
													 + "WHERE ResidentParkingLotID "
													 + "= " + residentParkingLotID;
					rs = stm.executeQuery(readResidentParkingLotSQL);
					while (rs.next()) {
						//sets main data
						ResidentParkingLot residentParkingLot = new ResidentParkingLot();
						residentParkingLot.setId(residentParkingLotID);
						residentParkingLot.setPostCode(rs.getString("PostCode"));
						residentParkingLot.setPostCodeDistrict(rs.getString("PostCodeDistrict"));
						
						//sets data of all resident areas of the resident parking lot
						ResidentAreaManager ram = new ResidentAreaManager();
						residentParkingLot.setResidentAreas(ram.readResidentAreas(residentParkingLot.getId()));
						
						return residentParkingLot;
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
		}
		return null;
	}

	/**
	 * reads out owner�s registrated resident parking lots from database
	 * returns an array with them
	 * @param owner
	 * @return
	 */
	public ArrayList<ResidentParkingLot> readResidentParkingLots(Owner owner) {
		
		if (owner != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					ArrayList<ResidentParkingLot> residentParkingLots = new ArrayList<>();
					
					String readResidentParkingLotSQL = "SELECT ResidentParkingLotID, "
													 + "PostCode, PostCodeDistrict "
													 + "FROM ResidentParkingLot "
													 + "WHERE OwnerID = " + owner.getId();
					rs = stm.executeQuery(readResidentParkingLotSQL);
					while (rs.next()) {
						ResidentParkingLot residentParkingLot = new ResidentParkingLot();
						
						//sets main data
						residentParkingLot.setId(rs.getInt("ResidentParkingLotID"));
						residentParkingLot.setPostCode(rs.getString("postCode"));
						residentParkingLot.setPostCodeDistrict(rs.getString("postCodeDistrict"));
						
						//sets data of all parkingAreas of the resident parking lot
						ResidentAreaManager ram = new ResidentAreaManager();
						residentParkingLot.setResidentAreas(ram.readResidentAreas(residentParkingLot.getId()));
						
						residentParkingLots.add(residentParkingLot);
					}
					return residentParkingLots;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}

	/**
	 * deletes resident parking lot and it�s data from database
	 * @param residentParkingLot
	 */
	public void deleteResidentParkingLotByID(int residentParkingLotID) {
		
		if (residentParkingLotID != 0) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//deletes resident parking lot�s main data
					String deleteResidentParkingLotSQL = "DELETE FROM ResidentParkingLot "
													   + "WHERE ResidentParkingLotID "
													   + "= " + residentParkingLotID;
					stm.executeUpdate(deleteResidentParkingLotSQL);
					
					//deletes all parking areas from resident parking lot
					ResidentAreaManager ram = new ResidentAreaManager();
					ram.deleteAllResidentAreasByID(residentParkingLotID);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * deletes all of owner�s resident parking lots and their data from database
	 * @param owner
	 */
	public void deleteAllResidentParkingLots (Owner owner) {
		
		if (owner != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteResidentParkingLotSQL = "DELETE FROM ResidentParkingLot "
													   + "WHERE ResidentParkingLotID "
													   + "= " + owner.getId();
					stm.executeUpdate(deleteResidentParkingLotSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	
	/** 
	 * return Arraylist of all ParkingLots, request input via searchform String postcode
	 * @param postcode
	 */
	
	
	public ArrayList<ResidentParkingLot> readResidentParkingLotPostCodes(String postcode) {
		
		if (postcode != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					ArrayList<ResidentParkingLot> residentParkingLots = new ArrayList<>();
					
					String readResidentParkingLotPostCodesSQL = "SELECT ResidentParkingLotID, PostCode, "
															  + "PostCodeDistrict "
															  + "FROM ResidentParkingLot "
															  + "WHERE PostCode = '"
															  + postcode + "'";
													 
					rs = stm.executeQuery(readResidentParkingLotPostCodesSQL);
					while (rs.next()) {
						ResidentParkingLot residentParkingLot = new ResidentParkingLot();
						
						//sets main data
						residentParkingLot.setId(rs.getInt("residentParkingLotID"));
						residentParkingLot.setPostCode(rs.getString("postCode"));
						residentParkingLot.setPostCodeDistrict(rs.getString("postCodeDistrict"));
						
						//sets data of all parkingAreas of the resident parking lot						
						
						residentParkingLots.add(residentParkingLot);
					}
					return residentParkingLots;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
}