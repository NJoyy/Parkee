<%@page import="controller.CustomerManager"%>
<%@page import="model.Customer"%>

<!DOCTYPE HTML>

<html>
<head>
	<title>Parkee - Kontodaten bearbeiten</title>
  
  	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/updateProfile.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
 	<link href="vendor/js/updateForm.js" rel="text/javascript">
</head>

<body>
	<!-- checks if the user is logged in as customer -->
	<jsp:include page="customer_CheckLogin.jsp"></jsp:include>

	<%
		Customer customer = (Customer) request.getSession().getAttribute("customer");
		CustomerManager cm = new CustomerManager();
	%>
 
	<div class="container">
	    <div class="login-container">
			<div id="output"></div>
			<div class="form-box">
				<form action="customer_UpdateProfileNow.jsp" method="post">
				<p>
					Ihr aktueller Benutzername lautet: <b> <%= customer.getName() %>
					</b>
				</p>
				<input name="name" type="text" placeholder="Benutzernamen eingeben"
					required="required"> <br>
			</div>
			<br>
			<div class="form-box">
				<p>
					Ihr aktuelle Email lautet: <b> <%= customer.getEmail() %>
					</b>
				</p>
				<input name="email" type="email" placeholder="Emailadresse eingeben" required="required">
				<button class="btn btn-info btn-block login" type="submit">Daten �ndern!</button>
				</form>
			</div>
		</div>       
	</div>  
</body>
</html>