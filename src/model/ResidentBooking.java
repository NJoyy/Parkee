package model;

/**
 * 
 * @author fabianM
 *
 */
public class ResidentBooking extends Booking {

	//attributes
	private ResidentArea residentArea;
	private String startDate;
	private String endDate;
	
	//getters and setters
	public ResidentArea getResidentArea() {
		return residentArea;
	}
	public void setResidentArea(ResidentArea residentArea) {
		this.residentArea = residentArea;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
}
