<%@page import="controller.OwnerManager"%>
<%@page import="model.Owner"%>

<%
	Owner owner = (Owner) request.getSession().getAttribute("owner");
	OwnerManager om = new OwnerManager();
	om.deleteOwner(owner);
%>

<jsp:forward page="logout.jsp"></jsp:forward>