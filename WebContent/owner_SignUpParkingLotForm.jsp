<html>
<head>
	<meta charset="UTF-8" />
	
	<title>Parkee - Registrierung Parkplatz</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/login.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,
	500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>

<body>
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

	<div class="container">
		<section>
			<div id="container_demo">
				<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
				<a class="hiddenanchor"></a>
				<div id="wrapper">
					<div id="signupParkingLot" class="animate form">
						<form action="owner_SignUpParkingLot.jsp" autocomplete="on"
							method="post">
							<h1>Parkplatz registrieren</h1>
							<p>Parkee bietet derzeit nur Lösungen zur Verwaltung von
								Bewohnerparkplätzen in Kommunen an.</p>
							<p>Hier können Sie einen Bewohnerparkplatz registrieren und
								anschließend in der Verwaltungsansicht die einzelnen
								Parkbereiche festlegen.</p>
							<p>
								<label for="postCode" class="postCode">Postleitzahl</label> <input
									id="postCode" name="postCode" required="required" type="text" pattern="[0-9.]+"
									placeholder="Bitte Postleitzahl angeben .." />
							</p>
							<p>
								<label for="postCodeDistrict" class="postCodeDistrict">Ort</label>
								<input id="postCodeDistrict" name="postCodeDistrict"
									required="required" type="text"
									placeholder="Bitte Ort angeben .." />
							</p>
							<br>
							<p class="ParkingLotSignUpButton">
								<input type="submit" value="Parkplatz registrieren"
									target="_blank">
							</p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>

</html>