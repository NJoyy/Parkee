package testController;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.CustomerManager;
import controller.LoginManager;
import controller.OwnerManager;
import controller.RegisterManager;
import model.Customer;
import model.Owner;

public class TestRegisterManager {
	
	RegisterManager rm = new RegisterManager();

	@Test
	public void testRegisterCustomer() {
		String name = "tester";
		String email = "testerCustomer@test.com";
		String password = "secret";
		
		rm.registerCustomer(name, email, password);
		
		CustomerManager cm = new CustomerManager();
		Customer customer = cm.readCustomerByEmail(email);
		
		assertEquals(customer.getId(), 1000002);
		assertEquals(customer.getName(), name);
		assertEquals(customer.getEmail(), email);
		assertEquals(customer.getPasswd(), password);
		
		cm.deleteCustomer(customer);
	}
	
	@Test
	public void testRegisterOwner() {
		String name = "tester";
		String email = "testerOwner@test.com";
		String password = "secret";
		
		rm.registerOwner(name, email, password);
		
		LoginManager lm = new LoginManager();
		Owner owner = lm.loginOwner(email, password);
		
		assertEquals(owner.getId(), 8000000);
		assertEquals(owner.getName(), name);
		assertEquals(owner.getEmail(), email);
		assertEquals(owner.getPasswd(), password);
		
		OwnerManager om = new OwnerManager();
		om.deleteOwner(owner);
	}
	
	@Test
	public void testIsEmailUsed() {
		String newEmail = "abcdef@abcdef.com";
		String usedEmail = "testcustomer@test.com";
		
		assertFalse(rm.isEmailUsed(newEmail));
		assertTrue(rm.isEmailUsed(usedEmail));
	}

}
