<!DOCTYPE HTML>
<%@page import="model.Owner"%>
<%@page import="controller.OwnerManager"%>

<html>
<head>
	<title>Parkee - Passwort �ndern</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/updateProfile.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,
	500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link href="vendor/js/updateForm.js" rel="text/javascript">
	
	<%
		Owner owner = (Owner) request.getSession().getAttribute("owner");
		OwnerManager om = new OwnerManager();
	%>
	
	<script type="text/javascript">
		// Check if the password is correct in the verify input
		function checkPass() {
			var pass1 = document.getElementById('passwd');
			var pass2 = document.getElementById('passwd_confirm');
			var message = document.getElementById('message');
			var colorRight = "#66cc66";
			var colorWrong = "#ff6666";
	
			if (pass1.value.length && pass2.value.length > 5) {
				if (pass1.value == pass2.value) {
					pass2.style.backgroundColor = colorRight;
					pass1.style.backgroundColor = colorRight;
					message.style.color = colorRight;
					message.innerHTML = "Passwort stimmt �berein!"
					$("#button_submit").show();
				} else {
					pass2.style.backgroundColor = colorWrong;
					pass1.style.backgroundColor = colorWrong;
					message.style.color = colorWrong;
					message.innerHTML = "Passwort stimmt nicht �berein!";
					$("#button_submit").hide()
				}
			} else {
				message.innerHTML = "Passwort muss mindestens 6 Zeichen lang sein!"
				message.style.color = colorWrong;
			}
		}
	
		/* function checkOldPass() {
			
			var passwd = // javacode get.passwd().value
			
			var passwd_old = document.getElementById('passwd_old');
				if(passwd == passwd_old.value){
					passwd_old.style.backgroundColor = colorRight;		
					
				}
				
			}
		 */
	</script>
</head>

<body>
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="owner_CheckLogin.jsp"></jsp:include>
	
	<div class="container">
		<div class="login-container">
			<div id="output"></div>
			<div class="avatar"></div>
			<div class="form-box">
				<form action="owner_ChangePasswordNow.jsp" method="post">
					<div class="password">
						<p>
							<label for="passwd_old" class="youpasswd">aktuelles
								Passwort eingeben</label> <input id="passwd_old" name="passwd_old"
								required="required" type="password" onKeyUp='checkOldPass();' />
						</p>
						<p>
							<label for="passwd" class="youpasswd">Neues Passwort
								setzen</label> <input id="passwd" name="passwd" required="required"
								type="password" onKeyUp='checkPass();' />
						</p>
						<p>
							<label for="passwd_confirm" class="youpasswd">Passwort
								best&auml;tigen</label> <input id="passwd_confirm" name="passwd_confirm"
								required="required" type="password" onKeyUp='checkPass();' />
						</p>
						<p>
							<span id='message'></span>
						</p>
					</div>
					<button class="btn btn-info btn-block login" id="button_submit"
						type="submit">Absenden!</button>
				</form>
			</div>
		</div>
	</div>
</body>

</html>